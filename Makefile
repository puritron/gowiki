SHELL=/bin/sh
OUTPUT=gowiki
#BUILD=$(shell git rev-parse HEAD)

SRC=wiki.go

LDFLAGS=-w -s
CFLAGS=-trimpath $(HOME)

.PHONY: all clean debug run
.NOTPARALLEL: clean

debug: LDFLAGS=
debug: CFLAGS=

all: $(OUTPUT)
debug: $(OUTPUT)

run: $(SRC)
	@go run $<

$(OUTPUT): $(SRC)
	@go build -gcflags '$(CFLAGS)' -ldflags="$(LDFLAGS)" -o $@
	
clean:
	-rm $(OUTPUT)