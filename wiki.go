// Copyright 2010 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"regexp"
)

type Page struct {
	Title string
	Body  []byte
	HTML  template.HTML
}

const templatePath = "tmpl"
const dataPath = "data"

var templateFiles = []string{"edit.html", "view.html"}
var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")
var validLink = regexp.MustCompile("\\[([a-zA-Z0-9]+)\\]")

var templates *template.Template

func (p *Page) save() error {
	filename := filepath.Join(dataPath, p.Title+".txt")
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func convertLink(src []byte) []byte {
	srcString := string(src)
	return []byte(fmt.Sprintf("<a href=\"/view/%s\">%s</a>", srcString, srcString))
}

func substBody(body []byte) []byte {
	return validLink.ReplaceAllFunc(body, func(matched []byte) []byte {
		return convertLink(validLink.FindSubmatch(matched)[1])
	})
}

func loadPage(title string) (*Page, error) {
	filename := filepath.Join(dataPath, title+".txt")
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	p.HTML = template.HTML(string(substBody(p.Body)))
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

// mapJoin applies filepath.Join on file slice, adding pathPrefix
func mapJoin(pathPrefix string, files []string) []string {
	retlist := make([]string, len(files))
	for i, file := range files {
		retlist[i] = filepath.Join(pathPrefix, file)
	}
	return retlist
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/view/FrontPage", http.StatusFound)
}

func main() {
	templates = template.Must(template.ParseFiles(mapJoin(templatePath, templateFiles)...))
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	http.ListenAndServe(":8080", nil)
}
