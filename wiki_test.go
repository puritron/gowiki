package main

import (
	"regexp"
	"testing"
)

func TestConvertLinks(t *testing.T) {
	var i interface{} = convertLink([]byte("test"))
	value, ok := i.([]byte)
	if !ok {
		t.Error("Wrong return type")
	}
	if string(value) != "<a href=\"/view/test\">test</a>" {
		t.Errorf("Wrong return value: Got: %s", value)
	}
}

func byteCompare(valid []byte, questionable []byte) int {
	// compare results
	failedOn := -1
	if len(valid) != len(questionable) {
		failedOn = 0
	} else {
		for i, b := range valid {
			if b != questionable[i] {
				failedOn = i
				break
			}
		}
	}
	return failedOn
}

func TestFindLink(t *testing.T) {
	validLink := regexp.MustCompile("\\[([a-zA-Z0-9]+)\\]")

	body := []byte("l1: [test], l2:[no mtch], l3: [OK]")
	validResp := []byte(`l1: <a href="/view/test">test</a>, l2:[no mtch], l3: <a href="/view/OK">OK</a>`)
	outbytes := validLink.ReplaceAllFunc(body, func(matched []byte) []byte {
		return convertLink(validLink.FindSubmatch(matched)[1])
	})

	if failedOn := byteCompare(validResp, outbytes); failedOn > -1 {
		t.Fatalf("Link converter failed. Want: %s, Got: %s. Failed on char: %d", string(validResp), string(outbytes), failedOn)
	}

}

func TestSubstBody(t *testing.T) {
	body := []byte("l1: [test], l2:[no mtch], l3: [OK]")
	validResp := []byte(`l1: <a href="/view/test">test</a>, l2:[no mtch], l3: <a href="/view/OK">OK</a>`)
	outbytes := substBody(body)
	if failedOn := byteCompare(validResp, outbytes); failedOn > -1 {
		t.Fatalf("Link converter failed. Want: %s, Got: %s. Failed on char: %d", string(validResp), string(outbytes), failedOn)
	}

}
